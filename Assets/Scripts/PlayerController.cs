﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private string[] moveAxes;
    [SerializeField]
    private KeyCode[] pickandDrop;
    [SerializeField]
    private float moveSpeed;
    
    private Rigidbody2D rigidbody;
    private Vector2 movement;
    private string caseSwitch;
    private bool onVegPoint = false;
    private bool onChopPoint = false;
    private bool onServePoint = false;
    private bool onThrowPoint = false;
    private enum OnPointStatus { onVegPoint , onChopPoint, onServePoint, onThrowPoint, onNoPoint};
    private OnPointStatus onPointStatus = OnPointStatus.onNoPoint;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Getting input for movement vector
        movement.x = Input.GetAxisRaw(moveAxes[0]);
        movement.y = Input.GetAxisRaw(moveAxes[1]);
       
        switch(onPointStatus)
        {
            case OnPointStatus.onVegPoint:
                 if (Input.GetKeyDown(pickandDrop[0]))
                {
                    PickUp();
                }
                else if (Input.GetKeyDown(pickandDrop[1]))
                {
                    Drop();
                }
                break;

            case OnPointStatus.onChopPoint:
                if (Input.GetKeyDown(pickandDrop[1]))
                {
                    Chop();
                }
                break;

            case OnPointStatus.onServePoint:
                if (Input.GetKeyDown(pickandDrop[1]))
                {
                    Serve();
                }
                break;

            case OnPointStatus.onThrowPoint:
                if (Input.GetKeyDown(pickandDrop[1]))
                {
                    ThrowInDustBin();
                }
                break;

        }

        
    }

    private void FixedUpdate()
    {
        //moving rigidbody according to movement vector
        rigidbody.MovePosition(rigidbody.position + movement * moveSpeed * Time.fixedDeltaTime);     
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        caseSwitch = collider.tag;
        switch(caseSwitch)
        {
            case "VegPoint":
                onVegPoint = true;
                onPointStatus = OnPointStatus.onVegPoint;
                break;

            case "ChopPoint":
                onChopPoint = true;
                onPointStatus = OnPointStatus.onChopPoint;
                break;

            case "ServePoinnt":
                onServePoint = true;
                onPointStatus = OnPointStatus.onServePoint;
                break;

            case "ThrowPoint":
                onThrowPoint = true;
                onPointStatus = OnPointStatus.onThrowPoint;
                break;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        caseSwitch = collider.tag;
        switch (caseSwitch)
        {
            case "VegPoint":
                onVegPoint = false;
                onPointStatus = OnPointStatus.onNoPoint;
                break;

            case "ChopPoint":
                onChopPoint = false;
                onPointStatus = OnPointStatus.onNoPoint;
                break;

            case "ServePoinnt":
                onServePoint = false;
                onPointStatus = OnPointStatus.onNoPoint;
                break;

            case "ThrowPoint":
                onThrowPoint = false;
                onPointStatus = OnPointStatus.onNoPoint;
                break;
        }
    }

    private void PickUp()
    {
        Debug.Log("picked");
    }

    private void Drop()
    {
        Debug.Log("dropped");
    }

    private void Chop()
    {
        Debug.Log("Chopped");
    }

    private void Serve()
    {
        Debug.Log("Served");
    }

    private void ThrowInDustBin()
    {
        Debug.Log("Threw");
    }
}
